#!/usr/bin/env python

import json
import os
import yaml
import sys

import aaargh

app = aaargh.App(description="Convert YAML to JSON and vice-versa.")

@app.cmd(help="Convert YAML to JSON")
@app.cmd_arg('-i', '--input-file', type=str, default=sys.stdin,
    help="YAML file to convert. Defaults to stdin.")
@app.cmd_arg('-o', '--output-file', type=str, default=sys.stdout,
    help="Path to write output to. Defaults to stdout.")
def to_json(input_file, output_file):
    """
    Convert a YAML file to JSON
    """
    try:
        # try treating input like a file
        with open(os.path.realpath(input_file), 'r') as input:
            input_data = yaml.load(input)
    except AttributeError:
        # must be stdin
        input_data = yaml.load(input_file)

    try:
        # try to write to stdout
        json.dump(input_data, output_file, sort_keys=True, indent=4, separators=(',', ': '))
    except AttributeError:
        with open(os.path.realpath(output_file), 'w+') as output:
            json.dump(input_data, output, sort_keys=True, indent=4, separators=(',', ': '))

        print("Successfully converted '%s' to '%s'" % (input_file, output_file))


@app.cmd(help="Convert JSON to YAML")
@app.cmd_arg('-i', '--input-file', type=str, default=sys.stdin,
    help="JSON file to convert. Defaults to stdin.")
@app.cmd_arg('-o', '--output-file', type=str, default=sys.stdout,
    help="Path to write output to. Defaults to stdout.")
def to_yaml(input_file, output_file):
    """
    Convert a JSON file to YAML
    """
    try:
        # try reading input like a file
        with open(os.path.realpath(input_file), 'r') as input:
            input_data = json.load(input)
    except AttributeError:
        # must be stdin
        input_data = json.load(input_file)

    try:
        # try to write to stdout
        yaml.safe_dump(input_data, output_file)
    except AttributeError:
        with open(os.path.realpath(output_file), 'w+') as output:
            yaml.safe_dump(input_data, output)

        print("Successfully converted '%s' to '%s'" % (input_file, output_file))


if __name__ == '__main__':
    app.run()