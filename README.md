Jayc - JSON to YAML and back
============================
Easily convert from JSON to YAML and back on the command line.

Convert YAML to JSON:

  `jayc.py to_json < my.yaml > my.json`

or convert JSON to YAML:

  `jayc.py to_yaml < my.json > my.yaml`

Run `jayc.py -h` to see help and get help on subcommands.

Installation
------------
Create a virtualenv:

  `virtualenv venv && source ./venv/bin/activate`

Then install the requirements with pip:

  `pip install -r requirements.txt`

Why?
----
Jayc makes it a breeze to work with CloudFormation templates in YAML. Simply
download a sample template from AWS and convert it to YAML. Neaten it up and
tailor it for your needs. You can use all of the awesomeness of PyYAML aliases
to write compact definitions which can then be converted back to JSON. Say hello
to a productive new you :-)